# SDR Lab Machines

This will be a separate repo to hold the machines building environments and a directory to allow the loading of particular labs. This separates the modification of a machine for use in mybinder.org, and the actual lab products.

* [![Binder](https://mybinder.org/badge.svg)](https://mybinder.org/v2/gl/jumson%2Fsdr-lab-machines/master)
* This is the base machine, based on the [jupyter/scipy-notebook image](https://jupyter-docker-stacks.readthedocs.io/en/latest/using/selecting.html)
* (user has passwordless sudo, so can always install specific things on top)
    * jupyterlab
    * numpy, scipy, matplotlib
    * bitarray
    * pywidgets
    * rise (for presentations)
    * nbconvert
    * pandoc 
    * bqplot
    * seaborn
    * nodejs
* To use the base machine,  !!
